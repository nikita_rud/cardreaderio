﻿using Card.IO;
using CardChecker.iOS;
using CardChecker.View;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CreditCardEntryView), typeof(CreditCardEntryViewRenderer))]
namespace CardChecker.iOS
{
    public class CreditCardEntryViewRenderer : PageRenderer
    {
        private bool bViewAlreadyDisappeared = false;

        private CreditCardEntryView ccPage;
        protected override void OnElementChanged(VisualElementChangedEventArgs e)
        {
            base.OnElementChanged(e);
            ccPage = e.NewElement as CreditCardEntryView;
        }


        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);

            // I don't know why ViewDidAppear keeps firing again, but we're able to shut it down 
            // by checking bViewAlreadyDisappeared.
            if (bViewAlreadyDisappeared) return;

            var paymentDelegate = new CardIOPaymentViewControllerDg(ccPage);

            // Create and Show the View Controller
            var paymentViewController = new CardIOPaymentViewController(paymentDelegate);

            paymentViewController.CollectExpiry = ccPage.cardIOConfig.RequireExpiry;
            paymentViewController.CollectCVV = ccPage.cardIOConfig.RequireCvv;
            paymentViewController.CollectPostalCode = ccPage.cardIOConfig.RequirePostalCode;
            paymentViewController.HideCardIOLogo = ccPage.cardIOConfig.HideCardIOLogo;
            paymentViewController.CollectCardholderName = ccPage.cardIOConfig.CollectCardholderName;

            if (!string.IsNullOrEmpty(ccPage.cardIOConfig.Localization)) paymentViewController.LanguageOrLocale = ccPage.cardIOConfig.Localization;
            if (!string.IsNullOrEmpty(ccPage.cardIOConfig.ScanInstructions)) paymentViewController.ScanInstructions = ccPage.cardIOConfig.ScanInstructions;

            // Not sure if this needs to be diabled, but it doesn't seem like something I want to do.
            paymentViewController.AllowFreelyRotatingCardGuide = false;

            // Display the card.io interface
            PresentViewController(paymentViewController, true, null);

        }

        public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidDisappear(animated);
            bViewAlreadyDisappeared = true;
        }
    }
}